// Importaciones necesarias en iMaster
import java.util.ArrayList;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

// Util (No modificar)
class JDBCUtilities {
    private static final String DATABASE_LOCATION = "ProyectosConstruccion.db";

    public static Connection getConnection() throws SQLException {
        String url = "jdbc:sqlite:"+DATABASE_LOCATION;

        return DriverManager.getConnection(url);
    }
}

// Remplace en adelante por las clases de sus archivos .java

// Vista
public class Vista {
    //Instanciamos el controlador de manera estatica
    public static final Controlador controlador = new Controlador();
    //generamos la vista para el requerimiento 1
    public static void vista_requerimiento_1() {

        try {
            //Lista para la consulta del requerimiento1
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_1();
            //Recorremos la lista para mostrar la informacion de las consultas
            for (int i = 0; i < proyectos.size(); i++) {
                //Datos para mostrar en Consola
                String info = "Fecha_Inicio: " + proyectos.get(i).getFecha_inicio();
                info += " - Numero_Habitaciones: " + proyectos.get(i).getNum_habitaciones();
                info += " - Numero_Banos: " + proyectos.get(i).getNum_banos();
                //Imprimir los datos iterador del array List
                System.out.println(info);

            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }
    //generamos la vista para el requerimiento 2
    public static void vista_requerimiento_2() {
        try {
            //Lista para la consulta del requerimiento2
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_2();
            //Recorremos la lista para mostrar la informacion de las consultas
            for (int i = 0; i < proyectos.size(); i++) {
                //Datos para mostrar en Consola
                String info = "Fecha_Inicio: " + proyectos.get(i).getFecha_inicio();
                info += " - Numero_Habitaciones: " + proyectos.get(i).getNum_habitaciones();
                info += " - Numero_Banos: " + proyectos.get(i).getNum_banos();
                info += " - Nombre_Lider: " + proyectos.get(i).getLider().getNombre();
                info += " - Apellido_Lider: " + proyectos.get(i).getLider().getApellido();
                info += " - Estrato_Proyecto: " + proyectos.get(i).getEstrato_proyecto();
                //Imprimir los datos iterado del array List
                System.out.println(info);

            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        

    }
    //generamos la vista para el requerimiento 3
    public static void vista_requerimiento_3() {
        try {
            //Lista para la consulta del requerimiento3
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_3();
            //Recorremos la lista para mostrar la informacion de las consultas
            for (int i = 0; i < proyectos.size(); i++) {
                //Datos para mostrar en Consola
                String info = "Total_Habitaciones: " + proyectos.get(i).getTotal_habitaciones();
                info += " - Constructora: " + proyectos.get(i).getNombre_constructora();
                
                //Imprimir los datos iterado del array List
                System.out.println(info);

            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        
    }

    public static void vista_requerimiento_4() {
        try {
            //Lista para la consulta del requerimiento2
            ArrayList<Lider> lideres = controlador.Solucionar_requerimiento_4();
            //Recorremos la lista para mostrar la informacion de las consultas
            for (int i = 0; i < lideres.size(); i++) {
                //Datos para mostrar en Consola
                String info = "Nombre_Lider: " + lideres.get(i).getNombre() + 
                " - Apellido_Lider: " + lideres.get(i).getApellido();
                //Imprimir los datos iterado del array List
                System.out.println(info);

            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        
    }

    public static void vista_requerimiento_5() {
        try {
            //Lista para la consulta del requerimiento5
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_5();
            //Recorremos la lista para mostrar la informacion de las consultas
            for (int i = 0; i < proyectos.size(); i++) {
                //Datos para mostrar en Consola
                String info = "Total_Habitaciones: " + proyectos.get(i).getTotal_habitaciones();
                info += " - Constructora: " + proyectos.get(i).getNombre_constructora();
                
                //Imprimir los datos iterado del array List
                System.out.println(info);

            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        
    }

}

// Controlador
public class Controlador {
    //Atributos de los DAO
    private final ProyectoDao proyectoDao;
    private final LiderDao liderDao;
    //MetodoConstuctor
    public Controlador() {
        this.proyectoDao = new ProyectoDao();
        this.liderDao = new LiderDao();
    }

    //Metodos de las clases
    public ArrayList<Proyecto> Solucionar_requerimiento_1() throws SQLException {
        //Llamamos los resultados de los query ejecutados
        return this.proyectoDao.query_requerimiento_1();
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_2() throws SQLException {
        return this.proyectoDao.query_requerimiento_2();
        
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_3() throws SQLException {
        return this.proyectoDao.query_requerimiento_3();
        
    }

    public ArrayList<Lider> Solucionar_requerimiento_4() throws SQLException {
        return this.liderDao.query_requerimiento_4();
        
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_5() throws SQLException {
        return this.proyectoDao.query_requerimiento_5();
        
    }

}

// Modelo
// VO
public class Lider{
    /*************
     * Atributos
     *************/
    private String nombre;
    private String apellido;

    //Constructor
    public Lider(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    //Consultores y Modificadores
    
    public Lider() {
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }


}

public class Proyecto {

    private String fecha_inicio;
    private int num_habitaciones;
    private String clasificacion;
    private int estrato_proyecto;
    private Lider lider;
    private String nombre_constructora;
    private int num_banos;
    private int total_habitaciones;
    private String nombre;
    private String primer_apellido;

    public Proyecto() {

    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public int getNum_habitaciones() {
        return num_habitaciones;
    }

    public void setNum_habitaciones(int num_habitaciones) {
        this.num_habitaciones = num_habitaciones;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public int getEstrato_proyecto() {
        return estrato_proyecto;
    }

    public void setEstrato_proyecto(int estrato_proyecto) {
        this.estrato_proyecto = estrato_proyecto;
    }

    public Lider getLider() {
        return lider;
    }

    public void setLider(Lider lider) {
        this.lider = lider;
    }

    public String getNombre_constructora() {
        return nombre_constructora;
    }

    public void setNombre_constructora(String nombre_constructora) {
        this.nombre_constructora = nombre_constructora;
    }

    public int getNum_banos() {
        return num_banos;
    }

    public void setNum_banos(int num_banos) {
        this.num_banos = num_banos;
    }

    public int getTotal_habitaciones() {
        return total_habitaciones;
    }

    public void setTotal_habitaciones(int total_habitaciones) {
        this.total_habitaciones = total_habitaciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    

    /*******************************
     * Consultores y modificadores
     ********************************/

    

}


// DAO
public class LiderDao {

    public ArrayList<Lider> query_requerimiento_4() throws SQLException {
        //Conexion con la BD
        Connection conexion = JDBCUtilities.getConnection();
        //Creamos un Arreglo para para almacenar objetos tipo proyecto
        ArrayList<Lider> lideres = new ArrayList<Lider>();
        //Consultas
        try {
            //Ejecuta la consulta query
            ResultSet query = conexion.createStatement().executeQuery(
                "SELECT Nombre, Primer_Apellido FROM Lider l INNER JOIN Proyecto p on l.ID_Lider = p.ID_Lider WHERE Constructora = \"Pegaso\" "
            );
            //while Recorre los resultados del Query
            while (query.next()){
                //Almacenamos los resultados del query en un objeto Lider
                Lider objLider = new Lider(query.getString("Nombre"), query.getString("Primer_Apellido"));
                //Agregamos el objeto al arrayList
                lideres.add(objLider);

            }
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e);
        }
        //Retornamos el ArrayList Lideres
        return lideres;
        
    }// Fin del método query_requerimiento_4

}

public class ProyectoDao {
    

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {
        //Conexion con la BD
        Connection conexion = JDBCUtilities.getConnection();
        //Creamos un Arreglo para para almacenar objetos tipo proyecto
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        //Consultas
        try {
            //Consulta para mostrar los datos de la BD
            String consulta = "SELECT Fecha_Inicio, Numero_Habitaciones, Numero_Banos FROM Proyecto p WHERE Constructora = 'Pegaso'; ";
            //Ejecutamos la consulta
            PreparedStatement statement = conexion.prepareStatement(consulta);
            ResultSet resultSet = statement.executeQuery();
            //Recorremos los registros en los VO especifivos
            while (resultSet.next()){
                //Almacenamos los resultados del query e un objeto Proyecto
                Proyecto objProyecto = new Proyecto();
                objProyecto.setFecha_inicio(resultSet.getString("Fecha_Inicio"));
                objProyecto.setNum_habitaciones(resultSet.getInt("Numero_Habitaciones"));
                objProyecto.setNum_banos(resultSet.getInt("Numero_Banos"));
                //Añadimos todos los objetos al array proyectos
                proyectos.add(objProyecto);
            }
            //cerramos el query
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            //Todo: handle exception
            System.err.println("Error en la Consulta "+ e);

        } finally {
            //cerramos la conexion a la BD
            if (conexion != null){
                conexion.close();
            }
        }
        //Retornamos el ArrayList la coleccion de VO
        return proyectos;

    }

    public ArrayList<Proyecto> query_requerimiento_2() throws SQLException {
        //Conexion con la BD
        Connection conexion = JDBCUtilities.getConnection();
        //Creamos un Arreglo para para almacenar objetos tipo proyecto
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        //Consultas
        try {
            //Ejecutamos el query de la consulta
            ResultSet query = conexion.createStatement().executeQuery(
                "SELECT p.Fecha_Inicio, p.Numero_Habitaciones, p.Numero_Banos, l.Nombre, l.Primer_Apellido, t.Estrato FROM Proyecto p INNER JOIN Lider l on l.ID_Lider = p.ID_Lider INNER JOIN Tipo t on t.ID_Tipo  = p.ID_Tipo WHERE Constructora = 'Pegaso' LIMIT 50;"
            );
            //Recorremos los registros en los VO especifivos
            while (query.next()){
                //Almacenamos los resultados del query e un objeto Proyecto
                Proyecto objProyecto = new Proyecto();
                objProyecto.setFecha_inicio(query.getString("Fecha_Inicio"));
                objProyecto.setNum_habitaciones(query.getInt("Numero_Habitaciones"));
                objProyecto.setNum_banos(query.getInt("Numero_Banos"));
                objProyecto.setEstrato_proyecto(query.getInt("Estrato"));
                //Creamos el Objeto Lider
                String nombre_lider = query.getString("Nombre");
                String apellido_lider = query.getString("Primer_Apellido");
                Lider objLider = new Lider(nombre_lider, apellido_lider);
                objProyecto.setLider(objLider);
                //Añadimos todos los objetos al array proyectos
                proyectos.add(objProyecto);
            }
            
        } catch (Exception e) {
            //Todo: handle exception
            System.out.println(e);

        } 
        //Retornamos el ArrayList proyecto
        return proyectos;
        
    }// Fin del método query_requerimiento_2


    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {
        //Conexion con la BD
        Connection conexion = JDBCUtilities.getConnection();
        //Creamos un Arreglo para para almacenar objetos tipo proyecto
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        //Consultas
        try {
            //Ejecutamos el query de la consulta
            ResultSet query = conexion.createStatement().executeQuery(
                "SELECT SUM(Numero_Habitaciones) as Total_Habitaciones, Constructora from Proyecto p GROUP BY Constructora;"
            );
            //Recorremos los registros en los VO especifivos
            while (query.next()){
                //Almacenamos los resultados del query e un objeto Proyecto
                Proyecto objProyecto = new Proyecto();
                objProyecto.setTotal_habitaciones(query.getInt("Total_Habitaciones"));
                objProyecto.setNombre_constructora(query.getString("Constructora"));
                
                //Añadimos todos los objetos al array proyectos
                proyectos.add(objProyecto);
            }
            
        } catch (Exception e) {
            //Todo: handle exception
            System.out.println(e);

        } 
        //Retornamos el ArrayList proyecto
        return proyectos;
        
    }// Fin del método query_requerimiento_3

    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException{
        //Conexion con la BD
        Connection conexion = JDBCUtilities.getConnection();
        //Creamos un Arreglo para para almacenar objetos tipo proyecto
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        //Consultas
        try {
            //Ejecutamos el query de la consulta
            ResultSet query = conexion.createStatement().executeQuery(
                "SELECT sum(Numero_Habitaciones) as Total_Habitaciones, Constructora from Proyecto p group by Constructora HAVING Total_Habitaciones > 200.0 ORDER BY Total_Habitaciones; "
            );
            //Recorremos los registros en los VO especifivos
            while (query.next()){
                //Almacenamos los resultados del query e un objeto Proyecto
                Proyecto objProyecto = new Proyecto();
                objProyecto.setTotal_habitaciones(query.getInt("Total_Habitaciones"));
                objProyecto.setNombre_constructora(query.getString("Constructora"));
                
                //Añadimos todos los objetos al array proyectos
                proyectos.add(objProyecto);
            }
            
        } catch (Exception e) {
            //Todo: handle exception
            System.out.println(e);

        } 
        //Retornamos el ArrayList proyecto
        return proyectos;
        
        
    }// Fin del método query_requerimiento_5
}